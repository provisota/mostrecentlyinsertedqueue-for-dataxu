import java.util.AbstractQueue;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

public class MostRecentlyInsertedQueue<E> extends AbstractQueue<E> {
    private final Queue<E> delegateQueue;
    private final int capacity;

    /**
     * Creates an {@code MostRecentlyInsertedQueue}
     * with the given (fixed) capacity.
     *
     * @param capacity the capacity of this queue
     * @throws IllegalArgumentException if {@code capacity < 1}
     */
    public MostRecentlyInsertedQueue(int capacity) {
        if (capacity <= 0) {
            throw new IllegalArgumentException("Capacity couldn't be less or equals than 0");
        }
        this.capacity = capacity;
        delegateQueue = new LinkedList<>();
    }

    @Override
    public Iterator<E> iterator() {
        return delegateQueue.iterator();
    }

    @Override
    public int size() {
        return delegateQueue.size();
    }

    @Override
    public boolean offer(E element) {
        if ((delegateQueue.size() == capacity)) {
            delegateQueue.poll();
        }
        return delegateQueue.offer(element);
    }

    @Override
    public E poll() {
        return delegateQueue.poll();
    }

    @Override
    public E peek() {
        return delegateQueue.peek();
    }
}
