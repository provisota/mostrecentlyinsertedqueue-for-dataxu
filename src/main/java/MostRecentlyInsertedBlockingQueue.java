import java.util.AbstractQueue;
import java.util.Collection;
import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class MostRecentlyInsertedBlockingQueue<E> extends AbstractQueue<E> implements BlockingQueue<E> {
    private final Queue<E> delegateQueue;

    /**
     * Creates an {@code MostRecentlyInsertedBlockingQueue}
     * with the given (fixed) capacity.
     *
     * @param capacity the capacity of this queue
     * @throws IllegalArgumentException if {@code capacity < 1}
     */
    public MostRecentlyInsertedBlockingQueue(int capacity) {
        if (capacity <= 0) {
            throw new IllegalArgumentException("Capacity couldn't be less or equals than 0");
        }
        delegateQueue = new ArrayBlockingQueue<>(capacity);
    }

    @Override
    public Iterator<E> iterator() {
        return delegateQueue.iterator();
    }

    @Override
    public int size() {
        return delegateQueue.size();
    }

    /**
     * From specification:<br>
     * 5) The queue must always accept new elements. If the queue is already full (Queue#size() == capacity),
     * the oldest element that was inserted (the head) should be evicted, and then the new element can be added at the tail.<br>
     * So we prevent to queue blocking by calling {@link #pollIfNecessary()} method.<br>
     *
     * @throws InterruptedException {@inheritDoc}
     * @throws NullPointerException {@inheritDoc}
     */
    @Override
    public synchronized void put(E element) throws InterruptedException {
        pollIfNecessary();
        ((ArrayBlockingQueue<E>) delegateQueue).put(element);
    }

    /**
     * From specification:<br>
     * 5) The queue must always accept new elements. If the queue is already full (Queue#size() == capacity),
     * the oldest element that was inserted (the head) should be evicted, and then the new element can be added at the tail.<br>
     * So we prevent to queue blocking by calling {@link #pollIfNecessary()} method.<br>
     *
     * @throws InterruptedException {@inheritDoc}
     * @throws NullPointerException {@inheritDoc}
     */
    @Override
    public synchronized boolean offer(E element, long timeout, TimeUnit unit) throws InterruptedException {
        pollIfNecessary();
        return ((ArrayBlockingQueue<E>) delegateQueue).offer(element, timeout, unit);
    }

    @Override
    public synchronized E take() throws InterruptedException {
        return ((ArrayBlockingQueue<E>) delegateQueue).take();
    }

    @Override
    public synchronized E poll(long timeout, TimeUnit unit) throws InterruptedException {
        return ((ArrayBlockingQueue<E>) delegateQueue).poll(timeout, unit);
    }

    @Override
    public int remainingCapacity() {
        return ((ArrayBlockingQueue<E>) delegateQueue).remainingCapacity();
    }

    @Override
    public synchronized int drainTo(Collection<? super E> collection) {
        return ((ArrayBlockingQueue<E>) delegateQueue).drainTo(collection);
    }

    @Override
    public synchronized int drainTo(Collection<? super E> collection, int maxElements) {
        return ((ArrayBlockingQueue<E>) delegateQueue).drainTo(collection, maxElements);
    }

    @Override
    public synchronized boolean offer(E element) {
        pollIfNecessary();
        return delegateQueue.offer(element);
    }

    @Override
    public synchronized E poll() {
        return delegateQueue.poll();
    }

    @Override
    public E peek() {
        return delegateQueue.peek();
    }

    /**
     * Poll if queue is already full
     */
    private void pollIfNecessary() {
        if (((ArrayBlockingQueue) delegateQueue).remainingCapacity() == 0) {
            delegateQueue.poll();
        }
    }
}
